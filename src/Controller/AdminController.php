<?php

namespace App\Controller;


use App\Entity\Jour;
use App\Entity\Module;
use App\Form\ModuleType;
use App\Entity\Categorie;
use App\Entity\Formateur;
use App\Entity\Formation;
use App\Entity\Stagiaire;
use App\Form\CategorieType;
use App\Form\FormationType;
use App\Form\StagiaireType;
use App\Repository\ModuleRepository;
use App\Repository\CategorieRepository;
use App\Repository\FormateurRepository;
use App\Repository\FormationRepository;
use App\Repository\StagiaireRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Dompdf\Dompdf;
use Dompdf\Options;


class AdminController extends AbstractController{

     /**
     * @Route("/admin", name="admin_panel")
     */
    public function showPanel()
    {  
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/admin/diplome/{id}/{formation_id}", name="edit_diplome")
     */
    public function editDiplome(Stagiaire $stagiaire, Formation $formation, Request $request, ObjectManager $om)
    {
 
            $dompdf = new Dompdf(['isPhpEnabled' => true]);
            
            $html = $this->renderView('admin/diplome.html.twig', [
                'stagiaire' => $stagiaire,
                'formation' =>$formation
            ]);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4','landscape');
            $dompdf->render();
            $dompdf->stream('mypdf.pdf', [
                'Attachment' => true
            ]);
    }

    /**
     * @Route("/admin/formation", name="add_formation")
     */
    public function createFormation(Formation $formation = null, Request $request, ObjectManager $om){
        $formation = new Formation();

        $form = $this->createFormBuilder($formation)
            ->add('name', TextType::class, [
                'label' => 'Nom de la formation'
            ])
            ->add('start_at', DateType::class, [
                'label' => 'Date de debut de formation',
                'widget' => 'single_text'
            ])
            ->add('end_at', DateType::class, [
                'label' => 'Date de Fin de formation',
                'widget' => 'single_text'
            ])
            ->add('placemax', IntegerType::class,[
                'label' => 'Nombre max de places',
            ]) 
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
            ])
            ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $formation->setName(ucfirst($formation->getName()));
            $om->persist($formation);
            $om->flush();

            return $this->render('admin/index.html.twig');
        }

        return $this->render('admin/index.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/create/formateur", name="add_formateur")
     */
    public function createFormateur(Formateur $formateur = null, Request $request, ObjectManager $om){
        $formateur = new Formateur();

        $form = $this->createFormBuilder($formateur)
            ->add('name', TextType::class, [
                'label' => 'Nom du formateur'
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom du formateur'
            ])
            ->add('birthday', DateType::class, [
                'label' => 'Date de Naissance',
                'widget' => 'single_text'
            ])

            ->add('phone', TextType::class,[
                'label' => 'Numero de telephone',
                'constraints' =>[new Regex([
                    'pattern' => '/^(?:\+33|0)[1-9](?:\d{2}){4}$/',
                    'message' => 'veuillez inserer un numero de telephone valide'
                ])]
            ])  
            ->add('email', EmailType::class)
            ->add('city', TextType::class,[
                'label' => 'Ville'
            ])
            ->add('categories   ', EntityType::class, [
                'class' => Categorie::class,
                'query_builder' => function (CategorieRepository $cr){
                    return $cr->createQueryBuilder('c')
                        ->orderBy('c.type', 'DESC');
                },
                'choice_label' => 'type',

            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
            ])
            ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $formateur->setName(ucfirst($formateur->getName()));
            $formateur->setFirstName(ucfirst($formateur->getFirstName()));
            $formateur->setCity(ucfirst($formateur->getCity()));
            $om->persist($formateur);
            $om->flush();

            return $this->render('admin/index.html.twig');
        }

        return $this->render('admin/index.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/create/stagiaire", name="add_stagiaire")
     */
    public function createStagiaire(Stagiaire $stagiaire = null, Request $request, ObjectManager $om){
        $stagiaire = new Stagiaire();

        $form = $this->createFormBuilder($stagiaire)
            ->add('name', TextType::class, [
                'label' => 'Nom du stagiaire'
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom du stagiaire'
            ])
            ->add('birthday', DateType::class, [
                'label' => 'Date de Naissance',
                'widget' => 'single_text'
            ])

            ->add('phone', TextType::class,[
                'label' => 'Numero de telephone',
                'constraints' =>[new Regex([
                    'pattern' => '/^(?:\+33|0)[1-9](?:\d{2}){4}$/',
                    'message' => 'veuillez inserer un numero de telephone valide'
                ])]
            ])  
            ->add('email', EmailType::class)
            ->add('city', TextType::class,[
                'label' => 'Ville'
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
            ])
            ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){


            $stagiaire->setName(ucfirst($stagiaire->getName()));
            $stagiaire->setFirstName(ucfirst($stagiaire->getFirstName()));
            $stagiaire->setCity(ucfirst($stagiaire->getCity()));
            $om->persist($stagiaire);
            $om->flush();

            return $this->render('admin/index.html.twig');
        }

        return $this->render('admin/index.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }

    
     /**
     * @Route("/admin/create/module", name="add_module")
     */
    public function createModule(Module $module = null, Request $request, ObjectManager $om){
        $module = new Module();

        $form = $this->createFormBuilder($module)
            ->add('name', TextType::class, [
                'label' => 'Nom du module'
            ])
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'query_builder' => function (CategorieRepository $cr){
                    return $cr->createQueryBuilder('c')
                        ->orderBy('c.type', 'DESC');
                },
                'choice_label' => 'type',   
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
            ])
            ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $module->setName(ucfirst($module->getName()));
            $om->persist($module);
            $om->flush();

            return $this->render('admin/index.html.twig');
        }

        return $this->render('admin/index.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/create/categorie", name="add_categorie")
     */
    public function createCategorie(Categorie $categorie = null, Request $request, ObjectManager $om){
        $categorie = new Categorie();

        $form = $this->createFormBuilder($categorie)
            ->add('type', TextType::class, [
                'label' => 'Categorie'
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
            ])
            ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $categorie->setType(ucfirst($categorie->getType()));
            $om->persist($categorie);
            $om->flush();

            return $this->render('admin/index.html.twig');
        }

        return $this->render('admin/index.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }

    /* linking entities */

    /**
     * @Route("/link/formation_module", name="link_formation_module")
     */
    public function linkFormationsModule(Jour $jour = null, Request $request, ObjectManager $om)
    {
        $jour = new Jour();

        $form = $this->createFormBuilder($jour)
            ->add('formation', EntityType::class,[
                'class' => Formation::class,
                'query_builder' => function(FormationRepository $fr){
                    return $fr->createQueryBuilder('f')
                        ->orderBy('f.name');
                },
                'choice_label' => 'name'
            ])
            ->add('module', EntityType::class,[
                'class' => Module::class,
                'query_builder' => function(ModuleRepository $mr){
                    return $mr->createQueryBuilder('m')
                        ->orderBy('m.name');
                },
                'choice_label' => 'name'
            ])
            ->add('nbJours', IntegerType::class,[
                'label' => 'Nombres de jours du module'
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();
        
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
            
                $om->persist($jour);
                $om->flush();
                return $this->redirectToRoute('admin_panel');
            }
    
            return $this->render('admin/index.html.twig', [
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/link/stagiaire_formation", name="link_stagiaire_formation")
     */
    public function linkStagiaireFormation(Request $request, ObjectManager $om){

    $form = $this->createFormBuilder()

        ->add('formation', EntityType::class,[
            'class' => Formation::class,
            'query_builder' => function(FormationRepository $fr){
                return $fr->createQueryBuilder('f')
                    ->orderBy('f.name');
            },
            'choice_label' => 'name',
            'mapped' => false
        ])
        ->add('stagiaire', EntityType::class,[
            'class' => Stagiaire::class,
            'query_builder' => function(StagiaireRepository $sr){
                return $sr->createQueryBuilder('s')
                    ->orderBy('s.name');
            },
            'mapped' => false
        ])
        ->add('envoyer', SubmitType::class, [
            'attr' => [ 'class' => 'btn btn-secondary btn-sm']
            ])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $forma = $form->get('formation')->getData();
            $nomformation = $forma->getName();
            $nbstagiaire = count($forma->getStagiaires());
            $nbplacemax = $forma->getPlaceMax();
            dump($forma);
            $formation = $this->getDoctrine()->getRepository(Formation::class)
            ->findOneBy(['name' => $nomformation]);
            
                if ($nbstagiaire < $nbplacemax){ 
                    $stagiaire = $form->get('stagiaire')->getData();
                    $formation = $form->get('formation')->getData();

                    $stagiaire->addFormation($formation);
                    $formation->addStagiaire($stagiaire);
                    $om->persist($stagiaire);
                    $om->persist($formation);
                    $om->flush();

                return $this->redirectToRoute('admin_panel');

            }    else {
                $this->addFlash("error","Formation pleine");
                return $this->render('admin/index.html.twig');
            }
     
        } 
        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }
    
           
    

    /**
     * @Route("/link/categorie_formateur", name="link_formateur_categorie")
     */
    public function linkFormateurCategorie(Request $request, ObjectManager $om)
    {
        
        $form = $this->createFormBuilder()

            ->add('formateur', EntityType::class,[
                'class' => Formateur::class,
                'query_builder' => function(FormateurRepository $fr){
                    return $fr->createQueryBuilder('f')
                        ->orderBy('f.name');
                },
                'mapped' => false
            ])
            ->add('categorie', EntityType::class,[
                'class' => Categorie::class,
                'query_builder' => function(CategorieRepository $cr){
                    return $cr->createQueryBuilder('c')
                        ->orderBy('c.type');
                },
                'choice_label' => 'type',
                'mapped' => false
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();
        
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $formateur = $form->get('formateur')->getData();
                $categorie = $form->get('categorie')->getData();

                $formateur->addCategory($categorie);
                $categorie->addFormateur($formateur);
                $om->persist($formateur);
                $om->persist($categorie);
                $om->flush();
                return $this->redirectToRoute('admin_panel');
            }
    
            return $this->render('admin/index.html.twig', [
                'form' => $form->createView(),
            ]);
    }

    /* Delete */

    /**
     *@Route("/delete/formation", name="formation_del") 
     */
    public function showDelFormation(Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder()
            ->add('formations', EntityType::class, [
                'class' => Formation::class,
                'query_builder' => function(FormationRepository $fr)
                {
                    return $fr->createQueryBuilder('f')
                        ->orderBy('f.name', 'DESC');
                },
                'choice_label' => 'name'
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formation = $form->get('formations')->getData();
            $manager->remove($formation);
            $manager->flush();
            return $this->redirectToRoute('admin_panel');
        }

        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     *@Route("/delete/formateur", name="formateur_del") 
     */
    public function showDelFormateur(Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder()
            ->add('formateur', EntityType::class, [
                'class' => Formateur::class,
                'query_builder' => function(FormateurRepository $sr)
                {
                    return $sr->createQueryBuilder('s')
                        ->orderBy('s.name', 'DESC');
                },
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formateur = $form->get('formateur')->getData();
            $manager->remove($formateur);
            $manager->flush();
            return $this->redirectToRoute('admin_panel');
        }

        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     *@Route("/delete/stagiaire", name="stagiaire_del") 
     */
    public function showDelStagiaire(Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder()
            ->add('stagiaire', EntityType::class, [
                'class' => Stagiaire::class,
                'query_builder' => function(StagiaireRepository $sr)
                {
                    return $sr->createQueryBuilder('s')
                        ->orderBy('s.name', 'DESC');
                },
                

            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stagiaire = $form->get('stagiaire')->getData();
            $manager->remove($stagiaire);
            $manager->flush();
            return $this->redirectToRoute('admin_panel');
        }

        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     *@Route("/delete/module", name="module_del") 
     */
    public function showDelModule(Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder()
            ->add('module', EntityType::class, [
                'class' => Module::class,
                'query_builder' => function(ModuleRepository $mr)
                {
                    return $mr->createQueryBuilder('m')
                        ->orderBy('m.name', 'DESC');
                },
                'choice_label' => 'name'
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $module = $form->get('module')->getData();
            $manager->remove($stagiaire);
            $manager->flush();
            return $this->redirectToRoute('admin_panel');
        }

        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     *@Route("/delete/categorie", name="categorie_del") 
     */
    public function showDelCategorie(Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder()
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'query_builder' => function(CategorieRepository $cr)
                {
                    return $cr->createQueryBuilder('c')
                        ->orderBy('c.type', 'DESC');
                },
                'choice_label' => 'type'
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [ 'class' => 'btn btn-secondary btn-sm']
              ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categorie = $form->get('categorie')->getData();
            $manager->remove($categorie);
            $manager->flush();
            return $this->redirectToRoute('admin_panel');
        }

        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/update/formation/{id}", name="update_formation")
     */
    public function updateFormation(Formation $formation, Request $request, ObjectManager $om)
    {
        $formations = $this->getDoctrine()
                    ->getRepository(Formation::class)
                    ->findAll();

        $form = $this->createFormBuilder($formation)
        ->add('name', TextType::class, [
            'label' => 'Nom de la formation'
        ])
        ->add('start_at', DateType::class, [
            'label' => 'Date de debut de formation',
            'widget' => 'single_text'
        ])
        ->add('end_at', DateType::class, [
            'label' => 'Date de Fin de formation',
            'widget' => 'single_text'
        ])
        ->add('placemax', IntegerType::class,[
            'label' => 'Nombre max de places',
        ]) 
        ->add('envoyer', SubmitType::class, [
            'attr' => [ 'class' => 'btn btn-secondary btn-sm']
        ])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $formation->setName(ucfirst($formation->getName()));
            $om->persist($formation);
            $om->flush();

            return $this->render('home/index.html.twig', [
                'title' => 'liste des formations',
                'formations' => $formations
            ]);
        }

        return $this->render('admin/update.html.twig', [
            'form'=> $form->createView()
        ]);
    }

        /**
     * @Route("/update/stagiaire/{id}", name="update_stagiaire")
     */
    public function updateStagiaire(Stagiaire $stagiaire, Request $request, ObjectManager $om)
    {
        $stagiaires = $this->getDoctrine()
                    ->getRepository(Stagiaire::class)
                    ->findAll();

        $form = $this->createFormBuilder($stagiaire)
        ->add('name', TextType::class, [
            'label' => 'Nom du stagiaire'
        ])
        ->add('firstname', TextType::class, [
            'label' => 'Prénom du stagiaire'
        ])
        ->add('birthday', DateType::class, [
            'label' => 'Date de Naissance',
            'widget' => 'single_text'
        ])

        ->add('phone', TextType::class,[
            'label' => 'Numero de telephone',
            'constraints' =>[new Regex([
                'pattern' => '/^(?:\+33|0)[1-9](?:\d{2}){4}$/',
                'message' => 'veuillez inserer un numero de telephone valide'
            ])]
        ])  
        ->add('email', EmailType::class)
        ->add('city', TextType::class,[
            'label' => 'Ville'
        ])
        ->add('envoyer', SubmitType::class, [
            'attr' => [ 'class' => 'btn btn-secondary btn-sm']
        ])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $stagiaire->setName(ucfirst($stagiaire->getName()));
            $stagiaire->setFirstName(ucfirst($stagiaire->getFirstName()));
            $stagiaire->setCity(ucfirst($stagiaire->getCity()));
            $om->persist($stagiaire);
            $om->flush();

            return $this->render('home/stagiaires.html.twig', [
                'stagiaires' => $stagiaires
            ]);
        }

        return $this->render('admin/update.html.twig', [
            'form'=> $form->createView()
        ]);
    }

}
