<?php

namespace App\Controller;

use DateTime;
use App\Entity\Module;
use App\Form\TestType;
use App\Entity\Categorie;
use App\Entity\Formateur;
use App\Entity\Formation;
use App\Entity\Stagiaire;
use App\Form\AdvancedSearchType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController{
    /**
     * @Route("/search", name="advanced_search")
     */
    public function advancedSearch(Request $request){
   
        $form = $this->createForm(AdvancedSearchType::class);
        $form->handleRequest($request);
        
         if ($form->isSubmitted() && $form->isValid()) {

            // Si categorie est null alors regardes les dates. 
            if ($form->get('categorie')->getData() == 'formation'){

                $from = $form->get('from')->getData();
                $to = $form->get('to')->getData();

                dump($from);
                dump($to);

                $between = $this->getDoctrine()->getRepository(Formation::class)
                ->findBetween($from, $to);

                return $this->render('search/search.html.twig', [
                    'form' => $form->createView(),
                    'between' => $between,
                ]);
            
                } elseif ($form->get('categorie')->getData() == 'categorie' ) {

                $results = $this->getDoctrine()
                    ->getRepository(Categorie::class)
                    ->findAllWithSearch(ucfirst($form->get('recherche')->getData()));

                return $this->render('search/search.html.twig', [
                    'results' => $results,
                    'form' => $form->createView()
                ]);
            } elseif ($form->get('categorie')->getData() == 'formation' ) {
                $results = $this->getDoctrine()
                    ->getRepository(Formation::class)
                    ->findAllWithSearch(ucfirst($form->get('recherche')->getData()));

                return $this->render('search/search.html.twig', [
                    'results' => $results,
                    'form' => $form->createView()
                ]);
            } elseif ($form->get('categorie')->getData() == 'formateur' ) {
                $results = $this->getDoctrine()
                    ->getRepository(Formateur::class)
                    ->findAllWithSearch(ucfirst($form->get('recherche')->getData()));

                return $this->render('search/search.html.twig', [
                    'results' => $results,
                    'form' => $form->createView()
                ]);
            } elseif ($form->get('categorie')->getData() == 'stagiaire' ) {
                $results = $this->getDoctrine()
                    ->getRepository(Stagiaire::class)
                    ->findAllWithSearch(ucfirst($form->get('recherche')->getData()));

                return $this->render('search/search.html.twig', [
                    'results' => $results,
                    'form' => $form->createView()
                    
                ]);
            } elseif ($form->get('categorie')->getData() == 'module' ) {
                $results = $this->getDoctrine()
                    ->getRepository(Module::class)
                    ->findAllWithSearch(ucfirst($form->get('recherche')->getData()));

                return $this->render('search/search.html.twig', [
                    'results' => $results,
                    'form' => $form->createView()
                   
                ]);
                
                
            } else 
            return $this->redirectToRoute('http://localhost:8000/search');
    } else
        return $this->render('search/search.html.twig',[
        'form' => $form->createView(),
        'request' => $request
    ]);
}


    /**
     * @Route("/search/result", name="search_result")
     */
    public function search(Request $request)
    {


        $search = $request->query->get('s');

        $results1 = $this->getDoctrine()
            ->getRepository(Categorie::class)
            ->findAllWithSearch(ucfirst($search));  
                    
        $results2 = $this->getDoctrine()
            ->getRepository(Formation::class)
            ->findAllWithSearch(ucfirst($search));
        
        $results3 = $this->getDoctrine()
            ->getRepository(Formateur::class)
            ->findAllWithSearch(ucfirst($search));

        $results4 = $this->getDoctrine()
            ->getRepository(Stagiaire::class)
            ->findAllWithSearch(ucfirst($search));

        $results5 = $this->getDoctrine()
            ->getRepository(Module::class)
            ->findAllWithSearch(ucfirst($search));
        
        $results = array_merge($results1, $results2, $results3, $results4, $results5);


        return $this->render('search/search.html.twig', [
            'results' => $results
        ]);
    }





}
    
    



