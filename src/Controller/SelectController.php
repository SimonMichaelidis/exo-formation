<?php

namespace App\Controller;

use App\Entity\Jour;
use App\Entity\Formateur;
use App\Entity\Formation;
use App\Entity\Stagiaire;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


    /**
     * @Route("/")
     */

class SelectController extends AbstractController{

    /**
     * @Route("/", name="show_all_formation")
     */
    public function showAllFormation(){

        $formations = $this->getDoctrine()->getRepository(Formation::class)
        ->findAll();

        return $this->render('home/index.html.twig', [
            'title' => 'liste des formations',
            'formations' => $formations 
        ]);
    }

     /**
     * @Route("/stagiaires", name="select_all_stagiaire")
     */
    public function showAllStagiaire(){

        $stagiaires = $this->getDoctrine()->getRepository(Stagiaire::class)
        ->findAll();
        return $this->render('home/stagiaires.html.twig', [
            'stagiaires' => $stagiaires
            ]);
    }

    /**
     * @Route("/select/formation/{id}", name="show_detail_formation")
     */
    public function showOneFormation($id){

        $jours = $this->getDoctrine()->getRepository(Jour::class)
        ->findBy(['formation' => $id]);
        $formation = $this->getDoctrine()->getRepository(Formation::class)->find($id);
        return $this->render('select/index.html.twig', [ 
            'title' => 'Détails',
            'title2' => 'Stagiaires',
            'formation' => $formation,
            'jours' => $jours
        ]);
    }
    

    /**
     * @Route("/select/stagaire/{id}", name="show_detail_stagiaire")
     */
    public function showOneStagiaire($id){

        $now = new \DateTime('now');
        $stagiaire = $this->getDoctrine()
                            ->getRepository(Stagiaire::class)
                            ->findOneBy(['id' => $id]);
        return $this->render('select/stagiaire.html.twig', [
                'now' => $now,
                'stagiaire' => $stagiaire
            ]); 
    }
    /**
     * @Route("/select/formateur/{id}", name="show_formateur")
     */
    public function showOneFormateur($id){

        $formateur = $this->getDoctrine()->getRepository(Formateur::class)
        ->find($id);
        return $this->render('select/formateur.html.twig', [
            'title' => 'Formateur',
            'formateur' => $formateur
            ]); 
    }

    
    
}

