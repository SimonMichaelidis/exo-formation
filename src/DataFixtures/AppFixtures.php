<?php

namespace App\DataFixtures;

use App\Entity\Module;
use App\Entity\Categorie;
use App\Entity\Formateur;
use App\Entity\Formation;
use App\Entity\Stagiaire;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

    class AppFixtures extends Fixture{

     /**
     * @Route("/")
     */
    public function load(ObjectManager $manager){
        // Liste des choses à ajouter
        
        // $tabStagiaire;
        // $tabFormation;
        // $tabModule;
        // $tabCategorie;

        $stagiaire = 30; 
        $formateur = 10;
        $i;
        $n;
      

        for ($i = 0; $i < $stagiaire; $i++){
            $tabStagiaire[] = 
                array('name' => 'Stagiaire '.$i, 'firstname' => '1', 'birthday' => '1991/06/01', 'phone' => '0686249293', 'email' => 'simon.michaelidis06@gmail.com', 'city' => 'Strasbourg');
        }

        for ($n = 0; $n < $formateur ; $n++){
            $tabFormateur[] = 
                array('name' => 'Formateur '.$n, 'firstname' => '1', 'birthday' => '1991/06/01', 'phone' => '0686249293', 'email' => 'simon.michaelidis06@gmail.com', 'city' => 'Strasbourg', 'cp' => '67000' );
        }

        for ($i = 0; $i < 10; $i++){
            $tabFormation[] = 
                array('name' => 'Formation '.$i , 'startAt' => '2019/06/01', 'duration' => $i * 30, 'placemax' => '20');
        }


       
        foreach($tabStagiaire as $row){
            // On crée la catégorie
            $stagiaire = new Stagiaire();
            $stagiaire->setName($row['name']);
            $stagiaire->setFirstName($row['firstname']);
            $stagiaire->setBirthday(new \DateTime($row ['birthday']));
            $stagiaire->setPhone($row['phone']);
            $stagiaire->setEmail($row['email']);
            $stagiaire->setCity($row['city']);
            $manager->persist($stagiaire);

        }

            foreach($tabFormateur as $row){
            // On crée la catégorie
            $formateur = new Formateur();
            $formateur->setName($row['name']);
            $formateur->setFirstName($row['firstname']);
            $formateur->setBirthday(new \DateTime($row ['birthday']));
            $formateur->setEmail($row['email']);
            $formateur->setCity($row['city']);
            $formateur->setCp($row['cp']);
            $formateur->setPhone($row['phone']);
            $manager->persist($formateur);
    
        }

        foreach($tabFormation as $row){
            // On crée la catégorie
            $formation = new Formation();
            $formation->setName($row['name']);
            $formation->setStartAt(new \DateTime ($row['startAt']));
            $formation->setDuration($row ['duration']);
            $formation->setPlacemax($row['placemax']);
            $manager->persist($formation);

        }
    
        // foreach($tabCategorie as $row){
        //     // On crée la catégorie
        //     $categorie = new Categorie();
        //     $categorie->setType($row['type']);
        //     $categorie->setFormateur($formateur);
        //     $manager->persist($categorie);

        // }
        
        // foreach($tabModule as $row){
        //     // On crée la catégorie
        //     $module = new Module();
        //     $module->setName($row['name']);
        //     $module->setCategorie($categorie);
        //     $manager->persist($module);
            
        // }

      

        // On déclenche l'enregistrement
        $manager->flush();
        }

       
}

