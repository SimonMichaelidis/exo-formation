<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Formation extends Fixture
{
    public function load(ObjectManager $manager)
    {
       
        for ($i = 0; $i < 10; $i++){
            $tabFormation[] = 
                array('name' => 'Formation '.$i, 'startAt' => '2019/06/01', 'duration' => $i * 30, 'placemax' => '20');
        }

        
        foreach($tabFormation as $row){
            // On crée la catégorie
            $formation = new Formation();
            $formation->setName($row['name']);
            $formation->setStartAt(new \DateTime ($row['startAt']));
            $formation->setDuration($row ['duration']);
            $formation->setPlacemax($row['placemax']);
            $manager->persist($formation);

        }

        $manager->flush();
    }
}
