<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $startAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Stagiaire", mappedBy="formations")
     */
    private $stagiaires;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Module")
     */
    private $modules;

    /**
     * @ORM\Column(type="integer")
     */
    private $placemax;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Jour", mappedBy="formation")
     */
    private $jours;

    /**
     * @ORM\Column(type="date")
     */
    private $endAt;
    

    public function __construct()
    {
        $this->stagiaires = new ArrayCollection();
        $this->jours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }


   

    public function getPlacemax(): ?int
    {
        return $this->placemax;
    }

    public function setPlacemax(int $placemax): self
    {
        $this->placemax = $placemax;

        return $this;
    }

    //  public function __toString(){
    //     return $this->getModules();

    // }

    /**
     * @return Collection|Stagiaire[]
     */
    public function getStagiaires(): Collection
    {
        return $this->stagiaires;
    }

    public function addStagiaire(Stagiaire $stagiaire): self
    {
        if (!$this->stagiaires->contains($stagiaire)) {
            $this->stagiaires[] = $stagiaire;
            $stagiaire->addFormation($this);
        }

        return $this;
    }

    public function removeStagiaire(Stagiaire $stagiaire): self
    {
        if ($this->stagiaires->contains($stagiaire)) {
            $this->stagiaires->removeElement($stagiaire);
            $stagiaire->removeFormation($this);
        }

        return $this;
    }

     /**
      * @return Collection|Jour[]
      */
     public function getJours(): Collection
     {
         return $this->jours;
     }

     public function addJour(Jour $jour): self
     {
         if (!$this->jours->contains($jour)) {
             $this->jours[] = $jour;
             $jour->setFormation($this);
         }

         return $this;
     }

     public function removeJour(Jour $jour): self
     {
         if ($this->jours->contains($jour)) {
             $this->jours->removeElement($jour);
             // set the owning side to null (unless already changed)
             if ($jour->getFormation() === $this) {
                 $jour->setFormation(null);
             }
         }

         return $this;
     }

     public function getEndAt(): ?\DateTimeInterface
     {
         return $this->endAt;
     }

     public function setEndAt(\DateTimeInterface $endAt): self
     {
         $this->endAt = $endAt;

         return $this;
     }


   
  
}
