<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdvancedSearchType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('recherche', SearchType::class, ['label' => false, 'required' => false ])
            ->add('categorie', ChoiceType::class, ['required' => false, 'label' => false, 'attr' => ['id' => 'radio'], 'placeholder' => false,
                'choices' => [ 
                    'Formateur' => 'formateur',
                    'Module' => 'module',
                    'Categorie' => 'categorie',
                    'Stagiaire' => 'stagiaire',
                    'Formation' => 'formation'                ], 
                'expanded' => true,  
            ])
            ->add('from', DateType::class, ['widget' => 'single_text', 'label' => 'Date de début', 'required' => false, 'attr' => ['class' => 'hidden catch' ]])
            ->add('to', DateType::class, ['widget' => 'single_text', 'label' => 'Date de fin', 'required' => false, 'attr' => ['class' => 'hidden catch' ]])
            ->add('search', SubmitType::class, ['attr' => ['class' => 'btn btn-light']])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}

    
