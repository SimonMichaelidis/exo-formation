<?php

namespace App\Repository;

use App\Entity\Formation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Formation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Formation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Formation[]    findAll()
 * @method Formation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry){
        parent::__construct($registry, Formation::class);

    }

    /**
     * @param string $term
     * @return Result[]
     */
    public function findAllWithSearch(string $term)
    {
        $Result = $this->createQueryBuilder('r');
        $Result->andWhere('r.name LIKE :term')
               ->setParameter('term', '%' .$term. '%');   
               

        return $Result
            ->orderBy('r.name', 'DESC')
            ->getQuery()
            ->getResult();   
    }

    public function findBetween($from, $to){

        $query = $this->createQueryBuilder('e');
        $query
        ->andWhere('e.startAt > :from')
        ->andWhere('e.endAt < :to')
        ->setParameter('from',$from)
        ->setParameter('to', $to);
                
        return $query
        ->getQuery()
        ->getResult();
    }

    public function findAllBefore($to){

        $query = $this->createQueryBuilder('e');
        $query->andWhere('e.endAt < :to')
              ->setParameter('to', $to);
                
        return $query
        ->getQuery()
        ->getResult();
    }

    // /**
    //  * @return Formation[] Returns an array of Formation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Formation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
